# Welcome

Hello, this is my project, you can use it to create XAPK files in order to use them
on your Android device, it uses zip compression to create xapks, it was created
using VB.NET.

## How it works?

Well, that's pretty easy, you just need to select the *APK* file, then select the
*Android* folder, here is a scheme, how the *Android* folder should look like:

```
Android:
    -obb:
        -*.obb
    -data:
        - the data folder is optional
```

Furthermore, you can select an icon to display on a XAPK installer by picking one
using the icon picker you have besides the APK selector.

Then you can press the ***Pack*** button to start exporting the XAPK, then you
will be prompted where to save the file, after that, download the file to your
Android device and use a XAPK installer to install it.

For more compatibility use [XAPK Installer](not yet)